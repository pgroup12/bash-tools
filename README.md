# BASH Tools



## What is it?
This project contains a few small tools for doing things on Linux easily I haven't found programs for. They're by no means complete or ready for any particular use, so you will have to modify them for your needs. Please use them carefully and only if you know what you're doing. Tried on Linux Mint 19.

## What is what?

GRecorder: Bash script for using GStreamer to detect and record all available (by time of launch) PulseAudio Sources

Ramdisk Manager: small script to make it super easy to create a temporary folder not written on disk, but on RAM. Requirements: having much more RAM space than the hardcoded number in the script (yes change it!). Depending on kernel configuration and remaining RAM tmpfs may swap! An alternative - ramfs is investigated if securely prevent any writes to disk.

More coming soon
